<?php

namespace Drupal\udemy_courses;

/**
 * Udemy Sync Courses Interface.
 */
interface UdemySyncCoursesInterface {

  /**
   * Udemy Courses List API.
   */
  public const UDEMY_COURSE_API='https://www.udemy.com/api-2.0/courses/';

  /**
   * Sync courses.
   *
   * @param int $page_size
   *   Page size.
   * @param int $current_page
   *   Current page.
   */
  public function sync(int $page_size, int $current_page = 1);

  /**
   * Get chunks.
   *
   * @param int $page_size
   *   Page size.
   *
   * @return int|array
   *   Return an integer if the page size is less than 100, otherwise an array.
   */
  public function getChunks(int $page_size);

}
