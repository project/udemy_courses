<?php

namespace Drupal\udemy_courses;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use League\Csv\Exception;

/**
 * Service description.
 */
class UdemySyncCourses implements UdemySyncCoursesInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an UdemySyncCourses object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ClientInterface $client, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->client = $client;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function sync(int $page_size, int $current_page = 1) {
    $settings = $this->configFactory->get('udemy_courses.settings');
    if (empty($settings->get('client_id')) || empty($settings->get('client_secret'))) {
      throw new Exception("The client ID or secret is empty.");
    }
    $options = [
      'headers' => [
        'Accept' => 'application/json, text/plain, */*',
        'Authorization' => 'Basic ' . base64_encode(implode(':', [
            $settings->get('client_id'),
            $settings->get('client_secret'),
          ])),
        'Content-Type' => 'application/json',
      ],
    ];
    $request = $this->client->get(self::UDEMY_COURSE_API . "?page=$current_page&page_size=$page_size", $options);
    if ($request->getStatusCode() === 200 && $response = $request->getBody()) {
      $result = Json::decode($response->getContents());
    }
  }

  /**
   * {@inerhitdoc}
   */
  public function getChunks(int $page_size) {
    if ($page_size <= 100) {
      return [$page_size];
    }
    $size = 100;
    $pieces = [];
    while ($page_size > 0) {
      $piece = min($size, $page_size);
      $pieces[] = $piece;
      $page_size -= $piece;
    }
    return $pieces;
  }

}
