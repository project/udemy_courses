<?php

namespace Drupal\udemy_courses\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Udemy Courses settings for this site.
 */
class UdemyCredentialsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'udemy_courses_udemy_credentials_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['udemy_courses.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('udemy_courses.settings');
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Client Id'),
      '#default_value' => $settings->get('client_id'),
      '#required' => TRUE,
    ];
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Client Secret'),
      '#default_value' => $settings->get('client_secret'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty('client_id')) {
      $form_state->setErrorByName('client_id', $this->t('This field cannot be empty.'));
    }
    if ($form_state->isValueEmpty('client_secret')) {
      $form_state->setErrorByName('client_secret', $this->t('This field cannot be empty.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('udemy_courses.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
