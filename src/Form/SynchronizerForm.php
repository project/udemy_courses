<?php

namespace Drupal\udemy_courses\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\udemy_courses\UdemySyncCoursesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Udemy Courses form.
 */
class SynchronizerForm extends FormBase {

  /**
   * Udemy Sync Courses service.
   *
   * @var \Drupal\udemy_courses\UdemySyncCoursesInterface
   */
  protected $udemySyncCourses;

  /**
   * SynchronizerForm constructor.
   *
   * @param \Drupal\udemy_courses\UdemySyncCoursesInterface $udemy_sync_courses
   *   The udemy sync courses service.
   */
  public function __construct(UdemySyncCoursesInterface $udemy_sync_courses) {
    $this->udemySyncCourses = $udemy_sync_courses;
  }

  /**
   * {@inerhitdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('udemy_courses.udemy_sync_courses')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'udemy_courses_synchronizer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['page'] = [
      '#type' => 'number',
      '#title' => $this->t('Current Page'),
      '#default_value' => 1,
      '#min' => 1,
    ];
    $form['page_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Page size'),
      '#required' => TRUE,
      '#default_value' => 10,
      '#min' => 1,
      '#max' => 1000,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync Courses'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty('page_size') || $form_state->getValue('page_size') < 1 || $form_state->getValue('page_size') > 1000) {
      $form_state->setErrorByName('page_size', $this->t('The page size must have values between 1 and 1000.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $current_page = $form_state->getValue('page') ?? 1;
    $page_size = $form_state->getValue('page_size');
    $chunks = $this->udemySyncCourses->getChunks($page_size);
    $batch_set = [];
    $batch_set['total'] = $page_size;
    foreach ($chunks as $chunk) {
      batch_set($this->udemySyncCourses->sync($chunk, $current_page));
      $current_page++;
    }
  }

}
